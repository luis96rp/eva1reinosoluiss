package edu.ec.istdab.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.mindrot.jbcrypt.BCrypt;

import edu.ec.istdab.dao.IUsuarioDAO;
import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IUsuarioService;

@Named
public class UsuarioServiceImpl implements IUsuarioService, Serializable
{
	
	@PersistenceContext(unitName = "evaPU")
	private EntityManager em;
	
	@EJB
	private IUsuarioDAO dao;
	
	@Override
	public Usuario login(Usuario us) { 
		// revisar
		// Registra el usuario 
		Usuario usuario = null;
		
		String clave = us.getClave();
		String claveHash = dao.traerPassHashed(us.getUsuario());
		try {
			if (!claveHash.isEmpty()) {
				if (BCrypt.checkpw(clave, claveHash)) { //Bcyrpt checkpw compara la clave encriptada con la clave desencriptada
					return dao.leerPorNombreUsuario(us.getUsuario()); // us.getusuario envia el nombre de usuario
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
		return null;
	}

	@Override
	public Integer registrar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		em.merge(t);
		return t.getPersona().getIdPersona();
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Usuario> listar() throws Exception {
		// TODO Auto-generated method stub
		return dao.listar();
	}

	@Override
	public Usuario listarPorId(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return dao.listarPorId(t);
	}
}
