package edu.ec.istdab.service;

import java.util.List;

public interface IService<T> {
	Integer registrar(T t) throws Exception; // t no necesita ser declarado, es un valor por defecto y generico, se declarara dependiendo de la clase u objeto con el que se vaya a necesitar de estos servicios
	Integer modificar(T t) throws Exception;
	Integer eliminar(T t) throws Exception;
	List<T> listar() throws Exception;
	T listarPorId(T t) throws Exception;
}
