package edu.ec.istdab.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.Init;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IUsuarioService;

@Named
@ViewScoped
public class IndexBean implements Serializable{
	
	private Usuario us;
	
	@Inject
	private IUsuarioService service;

	
	@PostConstruct
	public void init() {
		this.us = new Usuario();
	}
	
	
	public String login() { //No es un metodo de inicio de sesion, es un metodo para regresar al inicio
		String redireccion = " ";
		try {
			Usuario usuario = service.login(us); // service.login ejecuta el metodo login para inicio sesion
			
			if (usuario != null && usuario.getEstado().equalsIgnoreCase("A")) {
				//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Aviso","Correcto!")); //Mensaje que esta todo correcto el severity info
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", usuario);
				
				redireccion = "/protegido/roles?faces-redirect=true";
			}else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Aviso","Credenciales Incorrecto!")); //Mensaje
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Aviso",e.getMessage())); //Mensaje
		}
		return redireccion;
	}
	
	
	public Usuario getUs() {
		return us;
	}

	public void setUs(Usuario us) {
		this.us = us;
	}
}
